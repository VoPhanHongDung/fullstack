﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonymousMethod
{
    public delegate int Transformer(int x);
    class Program
    {
        static void Main(string[] args)
        {
            //anonymous method 
            Transformer tra = delegate (int x) { return x * x; };
            //compare lamda
            Transformer tra2 = x => x * x;


            //summary anonymous method can move the parameter declaration entirely ( bo qua tham so )
            Console.WriteLine(tra(10));
            Console.WriteLine(tra2(3));

            Console.ReadKey();
        }
    }
}
