﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorFunctions
{
    //OperatorFunctions cho phep lay gia tri cua obj da co vao cong them gia tri cho obj moi 
    // cong dung thu 2 Custom Implicit and Explicit Conversions
    class Program
    {
        public struct Note
        {
            private int value;

            public Note(int semitonesFromA)
            {
                value = semitonesFromA;
            }

            public static Note operator +(Note x, int semitones)
                => new Note(x.value + semitones);

            public void printValue()
            {
                Console.WriteLine(value);
            }

            // Convert to hertz
            public static implicit operator double(Note x)
                => 440 * Math.Pow(2, (double)x.value / 12);
            // Convert from hertz (accurate to the nearest semitone)
            public static explicit operator Note(double x)
                => new Note((int)(0.5 + 12 * (Math.Log(x / 440) / Math.Log(2))));

        }
        static void Main(string[] args)
        {
            Note noteA = new Note(100);
            Note noteC = noteA + 20;
            Note noteD = (Note) 3.4;
            double x = noteD;
            noteA.printValue();
            noteC.printValue();
            Console.WriteLine(x);
         

        Console.ReadKey();
        }
       
    }
}
