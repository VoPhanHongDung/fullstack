﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullableType
{
    class Program
    {
        static void Main(string[] args)
        {
            int? number = null;
            Console.WriteLine(number == null);

            int? x = 5; // implicit
            int y = (int) x; // explicit

            object o = "string";
            int? k = 0 as int?;
            Console.WriteLine(k.HasValue);

            Console.ReadKey();
        }
    }
}
