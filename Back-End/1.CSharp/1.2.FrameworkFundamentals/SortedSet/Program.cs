﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortedSet
{
    class Program
    {
        static void Main(string[] args)
        {
            var letters = new SortedSet<char>("the quick brown fox");
            foreach (char c in letters) Console.Write(c); // bcefhiknoqrtuwx
            Console.Write("\n");
            foreach (char c in letters.GetViewBetween('f', 'j'))
            Console.Write(c);
            Console.ReadKey();
        }
    }
}
