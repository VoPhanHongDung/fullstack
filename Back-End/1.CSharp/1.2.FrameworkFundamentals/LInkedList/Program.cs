﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LInkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            var tune = new LinkedList<string>();
            tune.AddFirst("do");
            tune.AddLast("so");

            tune.AddAfter(tune.First, "re"); // do - re- so
            tune.AddAfter(tune.First.Next, "mi"); // do - re - mi- so
            tune.AddBefore(tune.Last, "fa"); // do - re - mi - fa- so

            tune.RemoveFirst(); // re - mi - fa - so
            tune.RemoveLast(); // re - mi - fa

            LinkedListNode<string> miNode = tune.Find("mi");
            tune.Remove(miNode); // re - fa
            tune.AddFirst(miNode); // mi- re - fa

            foreach (string t in tune)
            {
                Console.WriteLine(t);
            }

            Console.ReadKey();
        }
    }
}
