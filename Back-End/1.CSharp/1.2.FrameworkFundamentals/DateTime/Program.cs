﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTime
{
    //: DateTime, DateTimeOffset, and TimeSpan
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(new TimeSpan(23,17,20,21));
            Console.WriteLine(new TimeSpan(2, 30, 0));

            var timeSpan = TimeSpan.FromHours(2) + TimeSpan.FromMinutes(30) + TimeSpan.FromDays(2);
            Console.WriteLine(timeSpan);

            var span = TimeSpan.FromDays(10) - TimeSpan.FromSeconds(1);
            Console.WriteLine(span);

            //we can illustrate the integer properties Days, Hours, Minutes,Seconds, and Milliseconds:
            TimeSpan nearlyTenDays = TimeSpan.FromDays(10) - TimeSpan.FromSeconds(1);
            Console.WriteLine(nearlyTenDays.Days); // 9
            Console.WriteLine(nearlyTenDays.Hours); // 23
            Console.WriteLine(nearlyTenDays.Minutes); // 59
            //In contrast, the Total... properties return values of type double describing the entire time span:            Console.WriteLine(nearlyTenDays.TotalDays);
            Console.WriteLine(nearlyTenDays.TotalHours);
            Console.WriteLine(string.Format("you have {0} hours to study",nearlyTenDays));

            //Formatting and parsing
            System.DateTime dt1 = System.DateTime.Now;
            string cannotBeMisparsed = dt1.ToString("yy-MM-dd");
            Console.WriteLine(cannotBeMisparsed);
            //System.DateTime dt2 = System.DateTime.Parse(cannotBeMisparsed);


            Console.ReadKey();
        }
    }
}
