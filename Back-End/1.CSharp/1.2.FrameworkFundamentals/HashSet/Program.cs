﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashSet
{
    class Program
    {
        static void Main(string[] args)
        {
            var letters = new HashSet<char>("the quick brown fox");
            Console.WriteLine(letters.Contains('t')); // true
            Console.WriteLine(letters.Contains('j')); // false
            foreach (char c in letters) Console.Write(c); // the quickbrownfx
            Console.Write("\n");
            letters.IntersectWith("aeiou"); //IntersectWith removes the elements that are not in both sets
            foreach (char c in letters) Console.Write(c); // euio
            Console.Write("\n");

            letters = new HashSet<char>("the quick brown fox");
            letters.ExceptWith("aeiou"); //ExceptWith removes the specified elements from the source set. 
            foreach (char c in letters) Console.Write(c); // th qckbrwnfx

            letters = new HashSet<char>("the quick brown fox");
            letters.SymmetricExceptWith("the lazy brown fox"); //SymmetricExceptWith removes all but the elements that are unique to one set or the
            other:
            foreach (char c in letters) Console.Write(c); // quicklazy
            Console.ReadKey();
        }
    }
}
