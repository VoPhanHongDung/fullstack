﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace String
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "hello Joey Tribbiani";
            Console.WriteLine(string.IsNullOrEmpty(str));

            //Searching within strings            Console.WriteLine(str.EndsWith("joey"));
            Console.WriteLine(str.Contains("Joey"));
            Console.WriteLine(str.StartsWith("hello"));
            Console.WriteLine(str.IndexOf("hello"));
            Console.WriteLine("pas5w0rd".IndexOfAny("0123456789".ToCharArray()));
            Console.WriteLine("abcde abcde".IndexOf("CD", 0,
                StringComparison.CurrentCultureIgnoreCase));

            //substring
            string subStr = str.Substring(0, 5);
            string subStrEnd5 = str.Substring(5);
            Console.WriteLine(subStr);
            Console.WriteLine(subStrEnd5);
            //insert and remove 
            str = str.Insert(5, " morning");
            Console.WriteLine(str);
            str = str.Remove(5,8);
            Console.WriteLine(str);
           
            //pad left and right
            Console.WriteLine("9".PadLeft(9, '?'));
            Console.WriteLine("12345".PadLeft(9, '*'));

            //trim remove specified characters and count char 
            Console.WriteLine("123 456 7 8 9\n".Trim().Length);
            
            //replace
            Console.WriteLine("to be done".Replace(" ", "-"));

            //split 
            string[] strs = "hello joey tribbiani , have a good day".Split();
            foreach (string s in strs)
            {
                Console.Write(s + "|");
            }
            Console.WriteLine("\n");

            //join chi dung cho array 
            string[] words = "The quick brown fox".Split();
            string together = string.Join(" ", words); // The quick brown fox
            Console.WriteLine(together);

            //concat same join dung cho chuoi string 
            string sentence = string.Concat("The", " quick", " brown", " fox");
            string strConact = string.Concat(together, sentence);
            Console.WriteLine(" here {0}",strConact);

            //join vs concat , concat dung cho string ghep 2 chuoi , join dung cho mang
            string[] abc = "a b c".Split();
            string def = "def";
            string common = string.Join(def, abc);
            Console.WriteLine(common);
            common = string.Concat(def, abc);
            Console.WriteLine(common);

            //String.Format and composite format strings
            string composite = "It's {0} degrees in {1} on this {2} morning";
            string sFormat = string.Format(composite, 1000, "Joey", DateTime.Now.DayOfWeek);
            Console.WriteLine(sFormat);

            string comDemo = "hello {0}";
            string comFormat = string.Format(comDemo, "Joey");
            Console.WriteLine(comFormat);

            string sDollar = $"It's cool this {DateTime.Now.DayOfWeek} morning";
            Console.WriteLine(sDollar);

            //format string have format
            string compositeFormat = "Name={0,-20} Credit Limit={1,5:C}";
            Console.WriteLine(string.Format(compositeFormat, "Mary", 500.43));
            Console.WriteLine(string.Format(compositeFormat, "Elizabeth", 20000));
            string sf = "Name=" + "Mary".PadRight(20) +
                       " Credit Limit=" + 500.ToString("C").PadLeft(15);
            Console.WriteLine(sf);
            string strPadleft3 = "joey".PadRight(7) + "tribbiani";
            Console.WriteLine(strPadleft3);

            //string compare 
            Console.WriteLine(string.Equals("foo", "FOO",StringComparison.OrdinalIgnoreCase));
            string compareFirst = "joey";
            string compareSecond = "Joey";
            Console.WriteLine(string.Equals(compareFirst,compareSecond,StringComparison.CurrentCultureIgnoreCase));

            //compareTo count char in string
            Console.WriteLine("Boston".CompareTo("Austin")); //1
            Console.WriteLine("Boston".CompareTo("Boston")); //0
            Console.WriteLine("Boston".CompareTo("Boston1")); //-1
            Console.WriteLine(string.Compare("foo", "foo111", true));

            // CultureInfo
            CultureInfo german = CultureInfo.GetCultureInfo("de-DE");
            int i = string.Compare("Müller", "Muller", false, german);
            Console.WriteLine(i);

            //StringBuilder StringBuilder, you can Append, Insert, Remove, and Replace sub‐strings without replacing the whole StringBuilder
            //A popular use of StringBuilder is to build up a long string by repeatedly calling Append.
            StringBuilder sb = new StringBuilder();
            sb.Append("hello");
            sb.Append(" joey");
            Console.WriteLine(sb);

            StringBuilder sbStringBuilder = new StringBuilder();
            for (int k = 0; k < 50; k++) sbStringBuilder.Append(k + ",");
            Console.WriteLine(sbStringBuilder.ToString());


      



            Console.ReadKey();
        }
    }
}
